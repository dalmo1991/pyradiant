from components import dcc_comp, html_comp
from db.mongo_connection import data
import dash_bootstrap_components as dbc

# TODO: make drier
def create_variable_selector(label, id, types=None, multiple=False, value=None):

    label_obj = html_comp.create_label(
        text=label,
        label_for=id,
    )

    # Select the variables for dropdown
    if 'patient_id' in types:
        variables = data.get_patients_ids()
    else:
        variables = data.get_variables(types)

    if not multiple:
        variables.insert(0, None)

    ddm = dcc_comp.create_dropdown(
        options=variables,
        multiple=multiple,
        id={'type': 'plot_variable', 'id': id},
        value=value,
    )

    return [label_obj, ddm]

def create_plot_options(plot_type, filters, plot_options):

    # For each type of plot I define what to visualize

    if plot_type == 'Histogram':
        children = [
            *create_variable_selector(
                label='Variable for x-axis',
                id='x_var',
                types=['tc_bool', 'tc_cat', 'tc_num'],
                value=plot_options['x_var'] if 'x_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for column facet',
                id='col_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['col_facet'] if 'col_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for row facet',
                id='row_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['row_facet'] if 'row_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for fill',
                id='fill',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['fill'] if 'fill' in plot_options else None,
            ),
        ]
        # TODO: Add slider for num bins in case of hist, smooting factor in case of dist
    elif plot_type == 'Scatter':
        children = [
            *create_variable_selector(
                label='Variable for x-axis',
                id='x_var',
                types=['tc_bool', 'tc_cat', 'tc_num'],
                value=plot_options['x_var'] if 'x_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for y-axis',
                id='y_var',
                types=['tc_bool', 'tc_cat', 'tc_num'],
                value=plot_options['y_var'] if 'y_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for column facet',
                id='col_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['col_facet'] if 'col_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for row facet',
                id='row_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['row_facet'] if 'row_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for color',
                id='color',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['color'] if 'color' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for symbol',
                id='symbol',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['symbol'] if 'symbol' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for size',
                id='size',
                types=['tc_num'],
                value=plot_options['size'] if 'size' in plot_options else None,
            ),
        ]
    elif plot_type == 'Bar':
        children = [
            *create_variable_selector(
                label='Variable for x-axis',
                id='x_var',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['x_var'] if 'x_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for y-axis',
                id='y_var',
                types=['tc_num'],
                value=plot_options['y_var'] if 'y_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for column facet',
                id='col_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['col_facet'] if 'col_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for row facet',
                id='row_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['row_facet'] if 'row_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for fill',
                id='fill',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['fill'] if 'fill' in plot_options else None,
            ),
        ]
    elif plot_type == 'Box':
        children = [
            *create_variable_selector(
                label='Variable for x-axis',
                id='x_var',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['x_var'] if 'x_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for y-axis',
                id='y_var',
                types=['tc_num'],
                value=plot_options['y_var'] if 'y_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for column facet',
                id='col_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['col_facet'] if 'col_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for row facet',
                id='row_facet',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['row_facet'] if 'row_facet' in plot_options else None,
            ),
            *create_variable_selector(
                label='Variable for color',
                id='color',
                types=['tc_bool', 'tc_cat'],
                value=plot_options['color'] if 'color' in plot_options else None,
            ),
        ]
    elif plot_type == 'NumericTimeVariant':
        children = [
            # x_var is time
            *create_variable_selector(
                label='Variable for y-axis',
                id='y_var',
                types=['tv_num'],
                value=plot_options['y_var'] if 'y_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Event to use as zero in the timeline',
                id='event_zero',
                types=['events'],
                value=plot_options['event_zero'] if 'event_zero' in plot_options else None,
            ),
            *create_variable_selector(
                label='Patient to overlap',
                id='patient_id',
                types=['patient_id'],
                multiple=True,
                value=plot_options['patient_id'] if 'patient_id' in plot_options else None,
            ),
        ]
    elif plot_type == 'CatBoolTimeVariant':
        children = [
            # x_var is time
            *create_variable_selector(
                label='Variable for y-axis',
                id='y_var',
                types=['tv_bool', 'tv_cat'],
                value=plot_options['y_var'] if 'y_var' in plot_options else None,
            ),
            *create_variable_selector(
                label='Event to use as zero in the timeline',
                id='event_zero',
                types=['events'],
                value=plot_options['event_zero'] if 'event_zero' in plot_options else None,
            ),
            *create_variable_selector(
                label='Patient to overlap',
                id='patient_id',
                types=['patient_id'],
                multiple=True,
                value=plot_options['patient_id'] if 'patient_id' in plot_options else None,
            ),
        ]
    else:
        raise KeyError('I don\'t know the plot {}'.format(plot_type))

    return dbc.Form(children)

def parse_plot_options(state_coltext):

    output = {}

    for opt in state_coltext:
        prop = opt['id']['id']
        var = opt['value']

        output[prop] = var

    return output