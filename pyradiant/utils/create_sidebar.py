from db.mongo_connection import data
import dash_html_components as html
from components import dcc_comp, html_comp
from .create_filters import create_filters
import dash_bootstrap_components as dbc

def create_sidebar():

    sidebar_children = [
        html.Div(
            [
                html.Details(
                    [
                        html.Summary(
                            html_comp.create_header_sidebar(
                                text='Create the filter',
                            ),
                        ),
                        dbc.Form(
                            [
                                html_comp.create_label(
                                    text='Select the variables for the filter',
                                    label_for='var_for_filter',
                                ),
                                dcc_comp.create_dropdown(
                                    options=data.get_variables(types=['tc_bool', 'tc_cat', 'tc_num']),  # TODO: sooner or later we will handle time variant
                                    # options=data.get_variables(types=['tc_bool', 'tc_cat', 'tc_num']), + ['Time'], # In case of timevariant, uncomment
                                    multiple=True,
                                    id='var_for_filter',
                                ),
                            ],
                        ),
                        html.Div(
                            dbc.Form(
                                id='filter_form_id'
                            )
                        ),
                    ]
                ),
                html.Details(
                    [
                        html.Summary(
                            html_comp.create_header_sidebar(
                                text='Options for the plot',
                            ),
                        ),
                        dcc_comp.create_dropdown(
                            options=[
                                'Histogram',
                                'Scatter',
                                'Bar',
                                'Box',
                                'NumericTimeVariant',
                                'CatBoolTimeVariant',
                            ],
                            multiple=False,
                            id='plot_type',
                        ),
                        html.Div(  # Populated with callback
                            id='plot_options_div',
                        ),
                    ],
                ),
            ],
            style={
                'height': '95%',
                'overflow-y': 'auto',
            },
        ),
        html.Div(
            html_comp.create_button(
                text='Plot!',
                id='create_the_magic',
                color='success',
                full_width=True,
                size='fill_div',
            ),
            style={
                'height': '5%',
            }
        ),
    ]

    return sidebar_children
