from dash.dependencies import Input, Output, State, ALL
from .create_filters import create_filters, parse_filters_div
from .create_plot_options import create_plot_options, parse_plot_options
from .create_plot import create_plot, create_empty_plot
import dash
from db.mongo_connection import data

def register_callbacks(dashboard):
    @dashboard.callback(
        Output('filter_form_id', 'children'),
        [
            Input('var_for_filter', 'value'),
        ],
        [
            State({'type': 'filter', 'id': ALL}, 'value'),
        ]
    )
    def create_filters_callback(variables, filter_div):

        out = create_filters(variables, dash.callback_context.states_list[0])
        if out is None:
            out = []
        return out

    @dashboard.callback(
        Output('plot_options_div', 'children'),
        [
            Input('plot_type', 'value'),
            Input({'type': 'filter', 'id': ALL}, 'value'),
        ],
        [
            State({'type': 'plot_variable', 'id': ALL}, 'value'),
        ],
    )
    def create_options_for_plot_callback(plot_type, filters, plot_options):

        filt = parse_filters_div(dash.callback_context.inputs_list[1])
        plot_opt = parse_plot_options(dash.callback_context.states_list[0])

        return create_plot_options(plot_type=plot_type, filters=filt, plot_options=plot_opt)


    @dashboard.callback(
        [
            Output('plot', 'figure'),
            Output('no_data_to_plot', 'is_open'),
            Output('warning_text', 'children'),
        ],
        [
            Input('create_the_magic', 'n_clicks'),
            Input('close_no_data_to_plot', 'n_clicks'),

        ],
        [
            State('plot_type', 'value'),
            State({'type': 'filter', 'id': ALL}, 'value'),
            State({'type': 'plot_variable', 'id': ALL}, 'value'),
            State('no_data_to_plot', 'is_open'),
        ]
    )
    def create_plot_callback(n_clicks, close_allert, plot_type, filters, plot_options, alert_open):

        filt = parse_filters_div(dash.callback_context.states_list[1])

        plot_options = parse_plot_options(dash.callback_context.states_list[2])

        for var in plot_options:
            if plot_options[var] == 'None':
                plot_options[var] = None

        text = 'The selected data are insufficient to create a plot!'

        if plot_options:
            if plot_type in ['Scatter', 'Bar']:
                if (plot_options['x_var'] is None) or (plot_options['y_var'] is None):
                    show_alert = True
                    i_can_plot = False
                else:
                    show_alert = False
                    i_can_plot = True
            elif plot_type in ['Histogram']:
                if (plot_options['x_var'] is None):
                    show_alert = True
                    i_can_plot = False
                else:
                    show_alert = False
                    i_can_plot = True
            elif plot_type in ['Box', 'NumericTimeVariant', 'CatBoolTimeVariant']:
                if (plot_options['y_var'] is None):
                    show_alert = True
                    i_can_plot = False
                else:
                    i_can_plot = True
                    show_alert = False
        else:
            if plot_type in ['TimeEvolution']:
                text = 'Time evolution plot cannot be made when filtering using the time!'
                show_alert = True
                i_can_plot = False
            else:
                show_alert = False
                i_can_plot = False

        # Dismiss the alert
        if alert_open:
            show_alert = False
            i_can_plot = False

        if i_can_plot:
            plot = create_plot(
                filters=filt,
                plot_type=plot_type,
                plot_options=plot_options,
            )
        else:
            plot = create_empty_plot()

        return plot, show_alert, text

    @dashboard.callback(
        Output('var_for_filter', 'options'),
        [
            Input({'type': 'filter', 'id': ALL}, 'value'),  # TODO: check if adding a filter trigers this
            # Input('var_for_filter', 'options')
        ]
    )
    def update_filters_options(filters):

        existing_filters = parse_filters_div(dash.callback_context.inputs_list[0])

        if 'Time' in existing_filters:
            # I can filter also for time variable parameters
            types = [
                'tc_bool',
                'tc_cat',
                'tc_num',
                'tv_bool',
                'tv_cat',
                'tv_num',
            ]
        else:
            # I can filter only for time constant parameters
            types = [
                'tc_bool',
                'tc_cat',
                'tc_num',
            ]

        variables = data.get_variables(types=types)
        # variables.append('Time') # Uncomment when we will be able to handle time variant

        return variables
