import plotly.express as px
import plotly.graph_objects as go
# from db.mongo_connection import data  # TODO: uncomment when running as app


def create_plot(filters, plot_type, plot_options):

    possible_options = [
        'x_var',
        'y_var',
        'col_facet',
        'row_facet',
        'fill',
        'color',
        'symbol',
        'size',
        # 'event_zero',
        # 'patient_id',
    ]

    columns = []
    for opt in possible_options:
        if opt in plot_options:
            if plot_options[opt] is not None:
                columns.append(plot_options[opt])
        else:
            plot_options[opt] = None

    aggregated, patient_specific = data.get_data(variables=columns,
                                                 filters=filters,
                                                 plot_type=plot_type,
                                                 time_variant_opt={
                                                     'event_zero': plot_options['event_zero'] if 'event_zero' in plot_options else None,
                                                     'patient_id': plot_options['patient_id'] if 'patient_id' in plot_options else None,
                                                     'start_plot': -50,  # TODO: hardcoded for simplicity
                                                     'end_plot': 50,  # TODO: hardcoded for simplicity
                                                 })

    if plot_type == 'Histogram':
        figure = px.histogram(
            data_frame=aggregated,
            x=plot_options['x_var'],
            facet_col=plot_options['col_facet'],
            facet_row=plot_options['row_facet'],
            color=plot_options['fill'],
        )
    elif plot_type == 'Scatter':
        figure = px.scatter(
            data_frame=aggregated,
            x=plot_options['x_var'],
            y=plot_options['y_var'],
            facet_row=plot_options['row_facet'],
            facet_col=plot_options['col_facet'],
            color=plot_options['color'],
            symbol=plot_options['symbol'],
            size=plot_options['size'],
        )
    elif plot_type == 'Bar':
        figure = px.bar(
            data_frame=aggregated,
            x=plot_options['x_var'],
            y=plot_options['y_var'],
            facet_col=plot_options['col_facet'],
            facet_row=plot_options['row_facet'],
            color=plot_options['fill'],
        )
    elif plot_type == 'Box':
        figure = px.box(
            data_frame=aggregated,
            x=plot_options['x_var'],
            y=plot_options['y_var'],
            facet_col=plot_options['col_facet'],
            facet_row=plot_options['row_facet'],
            color=plot_options['color'],
        )
    elif plot_type == 'NumericTimeVariant':
        figure = create_bloodstream(
            quantiles=aggregated,
            patients=patient_specific,
        )
    elif plot_type == 'CatBoolTimeVariant':
        raise NotImplementedError('Work in progress')
    else:
        raise KeyError('I don\'t know the plot {}'.format(plot_type))

    return figure


def create_empty_plot():
    figure = px.scatter()
    return figure


def create_bloodstream(quantiles, patients):
    fig = go.Figure()

    quantile_names = quantiles.columns
    pal = px.colors.sequential.Reds
    colors = [pal[0], pal[4], pal[4], pal[0]]

    fig.add_trace(go.Scatter(
        x=quantiles.index,
        y=quantiles[quantile_names[0]],
        mode='lines',
        line=dict(width=0.5, color=colors[0]),
        fill=None,
        opacity=0.5,
        showlegend=False,
    ))

    for idx, col in enumerate(quantile_names[1:]):
        fig.add_trace(go.Scatter(
            x=quantiles.index,
            y=quantiles[col],
            fill='tonexty',
            fillcolor=colors[idx],  # fill area between trace0 and trace1
            showlegend=False,
            mode='none'))

    fig.add_trace(go.Scatter(
        x=quantiles.index,
        y=quantiles[quantile_names[0]],
        mode='lines',
        showlegend=False,
        line=dict(width=0.5, color="black"),
    ))

    for idx, col in enumerate(quantile_names[1:]):
        fig.add_trace(go.Scatter(
            x=quantiles.index,
            y=quantiles[col],
            showlegend=False,
            fill=None,
            mode='lines',
            line=dict(width=0.5, color="black"),
        ))

    for pat in patients.keys():
        fig.add_trace(go.Scatter(
            x=patients[pat].index,
            y=patients[pat],
            mode='markers',
            name=pat,
        ))

    return fig


def create_heat_plot(quantities, patient):
    """
    The idea is to plot aggregated data as a heatmap and put patients over it.

    quantities: pd dataframe with index the time and column the values
    patient: pd dataframe with index the time and column the patient

    By design choice we plot a single patient
    """

    figure = px.imshow(quantities.transpose(), color_continuous_scale='Viridis')

    figure.add_trace(
        go.Scatter(
            x=(3.3, ),
            y=('V2', ),
            mode='markers',
            marker={
                'color': 'White',
                'size': 20,
                'symbol': 'cross',
                'line': {
                    'color': 'Black',
                    'width': 2,
                },
            }
        )
    )

    return figure


if __name__ == "__main__":

    import pandas as pd
    import numpy as np

    quantities = pd.DataFrame(np.random.randint(10, size=(10, 4)), index=np.arange(10), columns=['V1', 'V2', 'V3', 'V4'])
    patients = pd.DataFrame(np.random.randint(10, size=(10, 2)), index=np.arange(10), columns=['P1', 'P2'])

    figure = create_heat_plot(quantities, patients)

    figure.show()
