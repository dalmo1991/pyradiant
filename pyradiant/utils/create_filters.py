from db.mongo_connection import data
from components import dcc_comp, html_comp
import dash_bootstrap_components as dbc

def create_filters(variables, existing_filters):
    """
    Create filters

    Parameters
    ----------
    variables : list
        List of variables
    """

    existing_filters = parse_filters_div(existing_filters)
    # existing_filters = {}

    if variables is None:
        variables = []
    if isinstance(variables, str):
        variables = [variables]
    if 'Time' in variables:
        variables.remove('Time')
        has_time = True
    else:
        has_time = False

    filters_details = data.get_var_info(variables=variables)

    div_children = []

    for v in variables:
        # Create the label
        div_children.append(
            html_comp.create_label(
                text='Select the filter for {}'.format(v),
                label_for='filter_{}'.format(v)
            )
        )

        # Create the selector
        if v in existing_filters:
            value = existing_filters[v]
        else:
            value = None

        if filters_details[v]['type'] in ['tc_bool', 'tv_bool']:
            div_children.append(
                dcc_comp.create_radio_item(
                    options=filters_details[v]['value'],
                    value=value,
                    id={'type': 'filter', 'id': v},
                )
            )
        elif filters_details[v]['type'] in ['tc_cat', 'tv_cat']:
            div_children.append(
                dcc_comp.create_check_list(
                    options=filters_details[v]['value'],
                    value=value,
                    id={'type': 'filter', 'id': v},
                )
            )
        elif filters_details[v]['type'] in ['tc_num', 'tv_num']:
            div_children.append(
                dcc_comp.create_range_slider(
                    min_val=filters_details[v]['value'][0],
                    max_val=filters_details[v]['value'][1],
                    value=value,
                    id={'type': 'filter', 'id': v},
                )
            )
        else:
            raise KeyError()

    if has_time:
        if 'Time' in existing_filters:
            value = existing_filters['Time']
        else:
            value = None
        # Create the label
        div_children.append(
            html_comp.create_label(
                text='Select the filter for time',
                    label_for='filter_time'
            )
        )
        div_children.append(
            html_comp.create_label(
                text='Range of days',
                label_for='',
            )
        )
        number_of_days = 10  # TODO: it would be nice to get this from the db
        div_children.append(
            dcc_comp.create_range_slider(
                min_val=0,
                max_val=number_of_days,
                value=value['days_before_or_after_event'] if value is not None else None,
                id={'type': 'filter', 'id': 'days_before_or_after_event'},
                step=1,
            )
        )
        div_children.append(
            html_comp.create_label(
                text='Before or after?',
                label_for='',
            )
        )
        div_children.append(
            dcc_comp.create_radio_item(
                options=['Before', 'After'],
                id={'type': 'filter', 'id': 'before_or_after'},
                value=value['before_or_after'] if value is not None else None,
            )
        )
        div_children.append(
            html_comp.create_label(
                text='Select the event:',
                label_for='',
            )
        )
        div_children.append(
            dcc_comp.create_dropdown(
                options=data.get_variables(types=['events']),
                value=value['event_zero'] if value is not None else None,
                multiple=False,
                id={'type': 'filter', 'id': 'event_zero'}
            )
        )
        # div_children.append(
        #     html_comp.create_label(
        #         text='If filtering, keep record if:',
        #         label_for='',
        #         )
        #     )
        # div_children.append(
        #     dcc_comp.create_dropdown(
        #         options=['AllValuesInFilter', 'MinOneValueInFilter'],
        #         value=value['how_to_filter'] if value is not None else None,
        #         multiple=False,
        #         id={'type': 'filter', 'id': 'how_to_filter'}
        #     )
        # )
    return div_children

def parse_filters_div(state_context):

    filt = {}

    for f in state_context:

        if f['id']['id'] in ['event_zero', 'days_before_or_after_event', 'before_or_after']:
            variable = 'Time'
            if variable not in filt:
                filt[variable] = {f['id']['id']: f['value']}
            else:
                filt[variable][f['id']['id']] = f['value']

        else:
            variable = f['id']['id']
            value = f['value']
            filt[variable] = value

    return filt