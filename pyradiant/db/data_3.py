import pandas as pd
import numpy as np
import random

class Data():

    def __init__(self):
        pass # Nothing to initialize

    @staticmethod
    def get_variables(types=None):
        """
        This method returns the names of the variables present in the database.

        Parameters
        ----------
        types : list(str)
            List of types of variables to get. If None it returns all the
            variables.
            Possible types:
                - 'tc_bool'
                - 'tc_cat'
                - 'tc_num'
                - 'tv_bool'
                - 'tv_cat'
                - 'tv_num'
                - 'events'

        Returns
        -------
        list
            List with the names of the variables
        """

        variables = {
            'tc_bool': ['tc_bool_{}'.format(i) for i in range(3)],
            'tc_cat': ['tc_cat_{}'.format(i) for i in range(3)],
            'tc_num': ['tc_num_{}'.format(i) for i in range(3)],
            'tv_bool': ['tv_bool_{}'.format(i) for i in range(3)],
            'tv_cat': ['tv_cat_{}'.format(i) for i in range(3)],
            'tv_num': ['tv_num_{}'.format(i) for i in range(3)],
            'events': ['event_first_fever', 'event_admission'],
        }

        output = []
        if types is None:
            types = variables.keys()

        for t in types:
            for v in variables[t]:
                output.append(v)

        return output

    def get_choices(self, variables):
        """
        This method returns the uniques options in the variables. If time and
        event_zero are None then it works only with tcant variables, if
        both defined, it looks for a snapshot in the data.
        If numeric variables, it returns min and max.

        Parameters
        ----------
        variables : list(str)
            List of variables to return the choices.
        time : int
            Time stamp
        event_zero : str
            Event to start counting the days after

        Returns
        -------
        dict
            {variable: {type: 'type', value: choices}}
        """

        output = {}

        for v in variables:

            if v in self.get_variables(['tc_bool']):
                output[v] = {'type': 'tc_bool', 'value': [True, False]}
            elif v in self.get_variables(['tc_cat']):
                output[v] = {'type': 'tc_cat', 'value': ['C1', 'C2']}
            elif v in self.get_variables(['tc_num']):
                output[v] = {'type': 'tc_num', 'value': [0, 10]}
            elif v in self.get_variables(['tv_bool']):
                output[v] = {'type': 'tv_bool', 'value': [True, False]}
            elif v in self.get_variables(['tv_cat']):
                output[v] = {'type': 'tv_cat', 'value': ['C1', 'C2']}
            elif v in self.get_variables(['tv_num']):
                output[v] = {'type': 'tv_num', 'value': [0, 10]}

        return output

    def get_patients_ids(self):
        """
        This method returns the list of patients.

        Returns
        -------
        list(str)
            List of the patients in the database
        """
        return [str(i).zfill(4) for i in range(100)]

    def get_data(self, variables, filters, plot_type, bloodstream_options=None):
        """
        This method returns the data that are strictly needed for the plot.

        Parameters
        ----------
        filters : dict(str: list)
            Filters to apply to the data. Key: variable, value: options
        plot_options : dict(str: list)
            variables for the plot
        plot_type : str
            Type of plot

        Returns
        -------
        Depends on the type of plot

        """
        if plot_type == 'TimeEvolution':
            quantiles = pd.DataFrame(np.random.rand(15, 5)+np.array([5, 20, 30, 40, 65]),
                                     columns=['Q5', 'Q25', 'Q50', 'Q75', 'Q95'],
                                     index=range(15))

            num_pat = len(bloodstream_options['patients']) if isinstance(bloodstream_options['patients'], list) else 0

            observations = pd.DataFrame(np.random.randn(15, num_pat)*30+30,
                                        columns=bloodstream_options['patients'],
                                        index=range(15))

            return (quantiles, observations)
        else:
            data = {}
            for v in variables:
                if v in self.get_variables(['tc_bool']):
                    data[v] = random.choices([True, False], k=100)
                elif v in self.get_variables(['tc_cat']):
                    data[v] = random.choices(['C1', 'C2', 'C3'], k=100)
                elif v in self.get_variables(['tc_num']):
                    data[v] = np.random.rand(100)
                elif v in self.get_variables(['tv_bool']):
                    data[v] = random.choices([True, False], k=100)
                elif v in self.get_variables(['tv_cat']):
                    data[v] = random.choices(['C1', 'C2', 'C3'], k=100)
                elif v in self.get_variables(['tv_num']):
                    data[v] = np.random.rand(100)

            output = pd.DataFrame(data)

            return output

data = Data()