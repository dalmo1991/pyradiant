import requests
import yaml
import json
import os

# TODO: handle events

def linearize(keys, var_info, item, name):
    """
    This is a recursive function that reads all the levels of the database and
    extract the variables. Note that, since some fields of the database are not
    regular, they need to handled separately, using the functions:
    - handle_previous_conditions
    - handle_lab_test
    - handle_prescriptions

    Parameters
    ----------
    TODO
    """

    # Handle irregular db
    if name == 'previous_conditions':
        handle_previous_conditions(keys, var_info, item, name)
    elif name == 'lab_tests':
        handle_lab_test(keys, var_info, item, name)
    elif name == 'prescriptions':
        handle_prescriptions(keys, var_info, item, name)

    # Handle regular db
    elif isinstance(item, dict):
        for i in item:
            if len(name) == 0:
                linearize(keys, var_info, item[i], '{}'.format(i))
            else:
                linearize(keys, var_info, item[i], '{}.{}'.format(name, i))
    elif isinstance(item, list):
        for i, el in enumerate(item):
            linearize(keys, var_info, el, name)
    else:  # I can populate
        if isinstance(item, str):  # Categorical
            if name not in keys:
                keys[name] = 'cat'
                var_info[name] = {'type': None, 'value': [], 'path_to_value': name, 'path_to_timestamp': None, 'condition': None}
            if item not in var_info[name]['value']:
                var_info[name]['value'].append(item)
        elif isinstance(item, bool):  # Boolean
            if name not in keys:
                keys[name] = 'bool'
                var_info[name] = {'type': None, 'value': [True, False], 'path_to_value': name, 'path_to_timestamp': None, 'condition': None}
        elif isinstance(item, float) or isinstance(item, int):  # Numeric
            if name not in keys:
                keys[name] = 'num'
                var_info[name] = {'type': None, 'value': [], 'path_to_value': name, 'path_to_timestamp': None, 'condition': None}
            if len(var_info[name]['value']) == 0:
                var_info[name]['value'] = [item, item]
            else:
                if item < var_info[name]['value'][0]:
                    var_info[name]['value'][0] = item
                elif item > var_info[name]['value'][1]:
                    var_info[name]['value'][1] = item
        else:
            raise TypeError('Unsupported type: {}'.format(type(item)))

def handle_previous_conditions(keys, var_info, item, name):
    """
    This function opens the db for the variables contained in the "previous
    conditions" field.

    Parameters
    ----------
    TODO
    """

    if name not in keys:
        keys[name] = 'cat'
        var_info[name] = {'type': None, 'value': [], 'path_to_value': '{}.name'.format(name), 'path_to_timestamp': None, 'condition': None}

    for i in item:
        if i['name'] not in var_info[name]['value']:
            var_info[name]['value'].append(i['name'])

def handle_lab_test(keys, var_info, item, name):
    """
    This function opens the db for the variables contained in the "lab tests"
    field.

    Parameters
    ----------
    TODO
    """
    for test in item:
        for single_test in test['items']:
            par_name = single_test['parameter_key']
            par_value = single_test['value']

            if '{}.{}_value'.format(name, par_name) not in keys:
                keys['{}.{}_value'.format(name, par_name)] = 'num_tv'
                var_info['{}.{}_value'.format(name, par_name)] = {'type': None, 'value': [],
                                                                 'path_to_value': '{}.items.value'.format(name),
                                                                 'path_to_timestamp': '{}.timestamp'.format(name),
                                                                 'condition': '{}.items.parameter_key=={}'.format(name, par_name)}

            if '{}.{}_out_of_bounds'.format(name, par_name) not in keys:
                keys['{}.{}_out_of_bounds'.format(name, par_name)] = 'bool_tv'
                var_info['{}.{}_out_of_bounds'.format(name, par_name)] = {'type': None, 'value': [True, False],
                                                                         'path_to_value': '{}.items.is_out_of_bounds'.format(name),
                                                                         'path_to_timestamp': '{}.timestamp'.format(name),
                                                                         'condition': '{}.items.parameter_key=={}'.format(name, par_name)}

            if len(var_info['{}.{}_value'.format(name, par_name)]['value']) == 0:
                var_info['{}.{}_value'.format(name, par_name)]['value'] = [par_value, par_value]
            else:
                if par_value < var_info['{}.{}_value'.format(name, par_name)]['value'][0]:
                    var_info['{}.{}_value'.format(name, par_name)]['value'][0] = par_value
                elif par_value > var_info['{}.{}_value'.format(name, par_name)]['value'][1]:
                    var_info['{}.{}_value'.format(name, par_name)]['value'][1] = par_value

def handle_prescriptions(keys, var_info, item, name):
    """
    This function opens the db for the variables contained in the
    "prescriptions" field. So far, no data is kept

    Parameters
    ----------
    TODO
    """
    pass

if __name__ == '__main__':

    THIS_FILE_PATH = os.path.abspath(os.path.dirname(__file__))

    # Read user and password from yaml
    # TODO: change when on server

    with open(os.path.join(THIS_FILE_PATH, 'static_data', 'auth.yml'), 'r') as f:
        access = yaml.load(f, Loader=yaml.FullLoader)

    data_connection = requests.get('https://backend-demo.cobig19.com/patient',
                                auth=(access['user'], access['password']))

    if data_connection.status_code == 200:
        patients = data_connection.json()
    else:
        message = 'Cannot get data from db. Status code {}'.format(data_connection.status_code)
        raise ConnectionError(message)

    # TODO: consider refactoring. Not a priority but the linearize could create variables direcly, maybe
    keys = {}
    var_info = {}

    for pat in patients:
        linearize(keys, var_info, pat, '')

    # Look for time variant
    # Note: in theory, this works only with regular field
    #       for this reason the lab tests are threated separately in handle_lab_test
    time_variant = []

    for k in keys:
        splitted = k.split('.')
        if 'timestamp' in splitted:
            prefix = k.replace('.timestamp', '')
            time_variant.append(prefix)

    for k in keys:
        found = False
        for p in time_variant:
            if k[:len(p)] == p:
                var_info[k]['path_to_timestamp'] = '{}.timestamp'.format(p)
                keys[k] += '_tv'
                found = True
        if (not found) and ('_tv' not in keys[k]):
            keys[k] += '_tc'

    # Construct the "variables"

    variables = {
        'tc_bool': [],
        'tc_cat': [],
        'tc_num': [],
        'tv_bool': [],
        'tv_cat': [],
        'tv_num': [],
        'events': [],
    }

    for k in keys:
        if 'date_of' in k:
            variables['events'].append(k)
            var_info[k]['type'] = 'events'
        elif keys[k] == 'bool_tc':
            variables['tc_bool'].append(k)
            var_info[k]['type'] = 'tc_bool'
        elif keys[k] == 'cat_tc':
            variables['tc_cat'].append(k)
            var_info[k]['type'] = 'tc_cat'
        elif keys[k] == 'num_tc':
            variables['tc_num'].append(k)
            var_info[k]['type'] = 'tc_num'
        elif keys[k] == 'bool_tv':
            variables['tv_bool'].append(k)
            var_info[k]['type'] = 'tv_bool'
        elif keys[k] == 'cat_tv':
            variables['tv_cat'].append(k)
            var_info[k]['type'] = 'tv_cat'
        elif keys[k] == 'num_tv':
            variables['tv_num'].append(k)
            var_info[k]['type'] = 'tv_num'

    # Define single or double key (for Damiano)
    for k in var_info:
        if var_info[k]['condition'] is None:
            var_info[k]['field_type'] = 'single_key'
        else:
            var_info[k]['field_type'] = 'double_key'

    # Construct the translation
    # Read the existing
    TRANSLATIONS_FILE = os.path.join(THIS_FILE_PATH, 'static_data', 'translation.json')
    try:
        with open(TRANSLATIONS_FILE, 'r') as infile:
            translation = json.load(infile)
    except FileNotFoundError:
        translation = {}

    # Append new values
    for k in var_info:
        if k not in translation:
            translation[k] = None

    CHOICES_FILE = os.path.join(THIS_FILE_PATH, 'static_data', 'var_info.json')
    VARIABLES_FILE = os.path.join(THIS_FILE_PATH, 'static_data', 'variables.json')

    with open(CHOICES_FILE, 'w') as outfile:
        json.dump(var_info, outfile, sort_keys=True, indent=4)

    with open(VARIABLES_FILE, 'w') as outfile:
        json.dump(variables, outfile, sort_keys=True, indent=4)

    with open(TRANSLATIONS_FILE, 'w') as outfile:
        json.dump(translation, outfile, sort_keys=True, indent=4)
