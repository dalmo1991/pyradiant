import pandas as pd
import numpy as np

class Data():

    def __init__(self):
        self._data = self._construct_data(n_records=1000)

    @staticmethod
    def _construct_data(n_records):

        columns = ['Alive', 'IntensiveCare', 'Sick',
                   'Etnicity', 'PreviousConditions', 'HairColor', 'City', 'Department',
                   'Age', 'Height', 'Weight', 'BloodPar1', 'BloodPar2', 'BloodPar3', 'BloodPar4', 'BloodPar5', 'BloodPar6', 'BloodPar7', 'BloodPar8', 'BloodPar9']

        # First three variables are bool
        bool_var = [np.random.randint(0, 2, size=(n_records, 1)) for _ in range(3)]

        # Other five are categorical
        cat_var = [np.random.randint(0, i, size=(n_records, 1)) for i in range(3, 8)]

        # All the rest are numbers
        num_var = [np.random.rand(n_records, 1)*20 for _ in range(12)]

        # Put together
        var_values = [*bool_var, *cat_var, *num_var]
        var_values = np.array(var_values).squeeze()

        table = pd.DataFrame(var_values.transpose(), columns=columns)

        # Transform numbers to categories
        for i in range(3):
            table[columns[i]] = table[columns[i]].map({0: False, 1: True})

        for i in range(3, 8):
            table[columns[i]] = table[columns[i]].map({j: 'Cat_{}'.format(j) for j in range(0, i)})

        return table

    @staticmethod
    def get_variables(types=None):

        variables = {
            'bool': ['Alive', 'IntensiveCare', 'Sick'],
            'cat': ['Etnicity', 'PreviousConditions', 'HairColor', 'City', 'Department'],
            'num': ['Age', 'Height', 'Weight', 'BloodPar1', 'BloodPar2', 'BloodPar3', 'BloodPar4', 'BloodPar5', 'BloodPar6', 'BloodPar7', 'BloodPar8', 'BloodPar9'],
            'time': ['Time'],
            'time_variant': ['Temperature'],
            'event_zero': ['Admission'],
            'days_after_zero': list(range(10)),
            'interpolator': ['Linear'],
            'patient_id':['001', '002', '003'],
        }

        if types is None:
            types = list(variables.keys())

        output = []
        for t in types:
            if t not in variables:
                raise KeyError('')

            for v in variables[t]:
                output.append(v)

        return output

    def get_filter_options(self, variables):

        output = {}

        for v in variables:
            if v in self.get_variables(['bool']):
                output[v] = {'type': 'bool', 'value': [True, False]}
            elif v in self.get_variables(['cat']):
                categories = list(set(self._data[v]))
                categories.sort()
                output[v] = {'type': 'cat', 'value': categories}
            elif v in self.get_variables(['num']):
                min_val = self._data[v].min()
                max_val = self._data[v].max()
                output[v] = {'type': 'num', 'value': [min_val, max_val]}
            elif v in self.get_variables(['time']):
                output[v] = {'type': 'time', 'value': None}
            else:
                raise KeyError('Requested variable {} not in the database. Possible choices {}'.format(v, self.get_variables()))

        return output

    def filter_data(self, filter_entries, columns, plot_type):
        """
        Filter in the format
        {var: values}

        Example:
        {'bool_var': 'True', 'single_cat_opt': 'Opt',
         'multiple_cat_opt': ['Opt1', 'Opt2'],
         'numeriv_var': [0.4, 10.3]}
        """

        if plot_type == 'TimeEvolution':
            return pd.DataFrame()
        else:
            filtered_data = self._data.copy(deep=True)

            for v in filter_entries.keys():
                if v in self.get_variables(['bool']):
                    if filter_entries[v] == 'True':
                        filtered_data = filtered_data[filtered_data[v]]
                    else:
                        filtered_data = filtered_data[~filtered_data[v]]  # ~ should be elementwise not
                elif v in self.get_variables(['cat']):
                    if isinstance(filter_entries[v], str):
                        vals = [filter_entries[v]]
                    else:
                        vals = filter_entries[v]

                    filtered_data = filtered_data[filtered_data[v].isin(vals)]
                elif v in self.get_variables(['num']):
                    min_val = filter_entries[v][0]
                    max_val = filter_entries[v][1]
                    filtered_data = filtered_data[filtered_data[v].between(min_val, max_val, inclusive=True)]
                else:
                    raise KeyError('Requested variable {} not in the database. Possible choices {}'.format(v, self.get_variables()))

            return filtered_data[list(set(columns))]

data = Data()