import pandas as pd
import numpy as np


class Data():

    def __init__(self, num_samples):
        self._data = self._create_fake_data(num_samples=num_samples)

    @staticmethod
    def _create_fake_data(num_samples):

        patients_ids = [str(i).zfill(4) for i in range(num_samples)]

        # Create dataframe
        data = pd.DataFrame(index=patients_ids)

        # Create constant bool
        for i in range(3):
            data['const_bool_{}'.format(i)] = np.random.randint(0, 2, size=num_samples)
            data['const_bool_{}'.format(i)] = data['const_bool_{}'.format(i)].map({0: True, 1: False})

        # Create constant cat
        for i in range(3):
            data['const_cat_{}'.format(i)] = np.random.randint(0, 3+i, size=num_samples)
            data['const_cat_{}'.format(i)] = data['const_cat_{}'.format(i)].map({j: 'Cat_{}'.format(j) for j in range(0, 3+i)})

        # Create constant num
        for i in range(3):
            data['const_num_{}'.format(i)] = np.random.randn(num_samples)*20+50

        # Create variable bool
        for i in range(3):
            data['var_bool_{}'.format(i)] = None

            for pat in patients_ids:
                ts = pd.Series(np.random.randint(0, 2, size=10), index=range(10))
                ts = ts.map({0: True, 1: False})
                data.at[pat, 'var_bool_{}'.format(i)] = ts

        # Create variable cat
        for i in range(3):
            data['var_cat_{}'.format(i)] = None

            for pat in patients_ids:
                ts = pd.Series(np.random.randint(0, 3+i, size=10), index=range(10))
                ts = ts.map({j: 'Cat_{}'.format(j) for j in range(0, 3+i)})
                data.at[pat, 'var_cat_{}'.format(i)] = ts

        # Create variable num
        for i in range(3):
            data['var_num_{}'.format(i)] = None

            for pat in patients_ids:
                ts = pd.Series(np.random.randn(10)*20+50, index=range(10))
                data.at[pat, 'var_num_{}'.format(i)] = ts

        # Create events
        data['event_first_fever'] = np.random.randint(3, 5, size=num_samples)
        data['event_admission'] = 0

        return data

    @staticmethod
    def get_variables_names(types=None):
        """
        This method returns the names of the variables present in the database.

        Parameters
        ----------
        types : list(str)
            List of types of variables to get. If None it returns all the
            variables.
            Possible types:
                - 'const_bool'
                - 'const_cat'
                - 'const_num'
                - 'var_bool'
                - 'var_cat'
                - 'var_num'
                - 'events'

        Returns
        -------
        list
            List with the names of the variables
        """

        variables = {
            'const_bool': ['const_bool_{}'.format(i) for i in range(3)],
            'const_cat': ['const_cat_{}'.format(i) for i in range(3)],
            'const_num': ['const_num_{}'.format(i) for i in range(3)],
            'var_bool': ['var_bool_{}'.format(i) for i in range(3)],
            'var_cat': ['var_cat_{}'.format(i) for i in range(3)],
            'var_num': ['var_num_{}'.format(i) for i in range(3)],
            'events': ['event_first_fever', 'event_admission'],
        }

        output = []
        if types is None:
            types = variables.keys()

        for t in types:
            output += variables[t]

        return output

    def get_choices(self, variables, time=None, event_zero=None):
        """
        This method returns the uniques options in the variables. If time and
        event_zero are None then it works only with constant variables, if
        both defined, it looks for a snapshot in the data.
        If numeric variables, it returns min and max.

        Parameters
        ----------
        variables : list(str)
            List of variables to return the choices.
        time : int
            Time stamp
        event_zero : str
            Event to start counting the days after

        Returns
        -------
        dict
            {variable: {type: 'type', value: choices}}
        """

        output = {}

        for var in variables:
            if var in self.get_variables_names(['var_bool', 'var_cat', 'var_num']):
                if time is None or event_zero is None:
                    raise ValueError('I need to take a snapshot')
                else:
                    index = time + self._data[event_zero]

            if var in self.get_variables_names(['const_bool']):
                output[var] = {'type': 'const_bool', 'value': [True, False]}
            elif var in self.get_variables_names(['const_cat']):
                categories = list(set(self._data[var]))
                categories.sort()
                output[var] = {'type': 'const_cat', 'value': categories}
            elif var in self.get_variables_names(['const_num']):
                min_val = self._data[var].min()
                max_val = self._data[var].max()
                output[var] = {'type': 'const_num', 'value': [min_val, max_val]}
            elif var in self.get_variables_names(['var_bool']):
                output[var] = {'type': 'var_bool', 'value': [True, False]}
            elif var in self.get_variables_names(['var_cat']):
                content = []
                for i in self._data.index:
                    content.append(self._data.loc[i, var].loc[index.loc[i]])

                categories = list(set(content))
                categories.sort()
                output[var] = {'type': 'var_cat', 'value': categories}
            elif var in self.get_variables_names(['var_num']):
                content = []
                for i in self._data.index:
                    content.append(self._data.loc[i, var].loc[index.loc[i]])
                min_val = min(content)
                max_val = max(content)
                output[var] = {'type': 'var_num', 'value': [min_val, max_val]}

        return output

    def get_patients_ids(self):
        """
        This method returns the list of patients.

        Returns
        -------
        list(str)
            List of the patients in the database
        """
        return list(self._data.index)

    def get_data_to_plot(self, filters, plot_options, plot_type):
        """
        This method returns the data that are strictly needed for the plot.

        Parameters
        ----------
        filters : dict(str: list)
            Filters to apply to the data. Key: variable, value: options
        plot_options : dict(str: list)
            Variables for the plot
        plot_type : str
            Type of plot

        Returns
        -------
        Depends on the type of plot

        """
        pass