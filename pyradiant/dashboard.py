import os
import sys
path = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, path)
import dash
import dash_html_components as html
import dash_core_components as dcc
from utils import create_sidebar, create_plot
from utils.callbacks import register_callbacks
import dash_bootstrap_components as dbc

stylesheets = [
    {
        'rel': 'stylesheet',
        'href': os.path.join(path, 'assets', 'style.css'),
    },
    # dbc.themes.SANDSTONE,
    dbc.themes.SKETCHY,
]

dashboard = dash.Dash(__name__,
                      external_stylesheets=stylesheets,)

dashboard.config['suppress_callback_exceptions'] = True

dashboard.layout = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader('Warning', style={'color': 'red'}),
                dbc.ModalBody(id='warning_text'),
                dbc.ModalFooter(
                    dbc.Button('Close', id='close_no_data_to_plot', className='ml-auto'),
                )
            ],
            id="no_data_to_plot",
            backdrop='static',
            centered=True,
        ),
        html.Div(
            [
                html.H1('Welcome to the data exploration dashboard!'),
            ],
        ),
        html.Div(
            [
                html.Div(  # This div contains the side bar for filter selection
                    create_sidebar.create_sidebar(),
                    className='col-2',
                    style={
                        'height': '100%',
                    },
                ),
                html.Div(
                    dcc.Graph(
                        id='plot',
                        responsive=True,
                        className='fill_all_height',
                    ),
                    className='col',
                )
            ],
            className='fill_all_height row',
        ),
    ],
    className='full_vertical_height container-fluid',
)

register_callbacks(dashboard)

if __name__ == "__main__":

    dashboard.run_server(debug=True)