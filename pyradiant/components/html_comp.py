"""
General idea of this file is that we can customize here the components and
use the same style in all the dashboard, instead of changing element by
element.
"""

import dash_html_components as html
import dash_bootstrap_components as dbc

def create_button(text, id, color='primary', full_width=False, size=''):

    if size != 'fill_div':
        button = dbc.Button(text,
                            id=id,
                            color=color,
                            block=full_width,
                            size=size,
                            className='mr-1')
    else:
        button = dbc.Button(text,
                            id=id,
                            color=color,
                            block=full_width,
                            className='mr-1',
                            style={
                                'width': '90%',
                                'height': '90%',
                                'margin' : '5%',
                            },)


    return button

def create_label(text, label_for):

    label = dbc.FormGroup(
        html.Label(
            children=text,
            htmlFor=label_for
        )
    )

    return label

def create_header_sidebar(text):

    header = html.H3(
        children=text,
        className='sidebar_header'
    )

    return header