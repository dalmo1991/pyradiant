"""
General idea of this file is that we can customize here the components and
use the same style in all the dashboard, instead of changing element by
element.
"""

import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import dash_bootstrap_components as dbc

def create_dropdown(options, multiple, id, value=None):

    for i, el in enumerate(options):
        if not isinstance(el, dict):
            options[i] = {'label': str(el), 'value': str(el)}

    if multiple:
        # value = None  # TODO: this may not work
        pass
    else:
        if value is None:
            value = str(options[0]['value'])

    ddm = dbc.FormGroup(
        dcc.Dropdown(
            options=options,
            value=value,
            multi=multiple,
            id=id,
        )
    )

    return ddm

def create_range_slider(min_val, max_val, id, step=0.05, value=None):

    if value is None:
        value = [min_val, max_val]

    rs = dbc.FormGroup(
        dcc.RangeSlider(
            min=min_val,
            max=max_val,
            allowCross=False,
            marks={min_val: str(np.round(min_val, 2)), max_val: str(np.round(max_val, 2))},
            step=step,
            value=value,
            id=id,
            tooltip={'always_visible': False, 'placement': 'top'},
            updatemode='drag',
        )
    )

    return rs

def create_slider(min_val, max_val, id, step=0.05, value=None):

    if value is None:
        value = min_val

    rs = dbc.FormGroup(
        dcc.Slider(
            min=min_val,
            max=max_val,
            marks={min_val: str(np.round(min_val, 2)), max_val: str(np.round(max_val, 2))},
            step=step,
            value=value,
            id=id,
            tooltip={'always_visible': True, 'placement': 'top'},
            updatemode='drag',
        )
    )

    return rs

def create_radio_item(options, id, value=None):

    if value is None:
        value = str(options[0])
    ri = dbc.FormGroup(
        dbc.RadioItems(
            options=[{'label': str(opt), 'value': str(opt)} for opt in options],
            value=value,
            id=id,
        )
    )

    return ri

def create_check_list(options, id, value=None):

    if value is None:
        value = []

    cl = dbc.FormGroup(
        dbc.Checklist(
            options=[{'label': str(opt), 'value': str(opt)} for opt in options],
            value=value,
            id=id,
        )
    )

    return cl