1. Create ubuntu EC2

2. Give access groups

3. Download ssh key pair

4. Move to dir where key is saved

5. Change permission on the key file

   `chmod 400 key.pem`

6. Copy public DNS (IPv4)

7. Connect ot the server

   `ssh -i key.pem ubuntu@DNS`

8. Clone the repo

   `git clone https://dalmo1991@bitbucket.org/dalmo1991/pyradiant.git`

9. Checkout to the deployment branch

10. Update/Upgrade system

    `sudo apt update`

    `sudo apt upgrade`

11. Install python 3.7.7

    `sudo apt install python3.7`

12. Create virtual environment with python 3.7.7

    `sudo apt install python3-pip`

    `sudo apt install virtualenv`

    `virtualenv -p /usr/bin/python3.7 covid-hack`

13. Activate venv

    `source covid-hack/bin/activate`

14. Install

    `pip3.7 install -r requirements`

15. Disable firewall

    `sudo ufw disable`

16. Run the dashboard

17. App accessible at

    `IP:8050`