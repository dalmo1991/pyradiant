import plotly.graph_objects as go
import numpy as np

x = [1, 2, 3, 4]
yt1 = [5, 5, 5, 5]
yt2 = [3, 3, 3, 3]



trace2 = go.Scatter(
    name='trace2',
    x=x,
    y=yt2,
    fill='tonexty',
)

trace1 = go.Scatter(
    name='trace1',
    x=x,
    y=yt1,
    showlegend=True,
    opacity=1,
    mode='lines+markers',  # Options 'lines' 'markers' 'text' or a sum of them 'lines+markers'
    marker={
        'symbol': 'circle-open',
        'size': 20,
        'color': 'green',
    },
    line={
        'color': 'red',
        'width': 5,
        'dash': 'dash',
    },
    fill='tonexty',
)
fig = go.Figure(data=[trace1, trace2])

fig.show()